use clap::Parser;
use reqwest::header::AUTHORIZATION;
use serde::{Deserialize, Serialize};
use std::fs;

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Args {
    #[clap(help = "ICAO or IATA station code")]
    airport: String,
}

#[derive(Serialize, Deserialize)]
struct Config {
    avwx_auth_token: String,
}

#[derive(Deserialize)]
struct AVWXResponse {
    raw: String,
}

fn main() {
    let mut config_file_location = dirs::config_dir().unwrap();
    config_file_location.push("metars.toml");
    let config_file_location = config_file_location;

    let args = Args::parse();
    let client = reqwest::blocking::Client::new();
    if let Ok(config_file) = fs::read_to_string(&config_file_location) {
        let config = toml::from_str::<Config>(&config_file).expect(&format!(
            "Invalid config file - edit {:?} to include 'avwx_auth_token'",
            config_file_location
        ));
        let response = client
            .get(format!("https://avwx.rest/api/metar/{}", args.airport))
            .header(AUTHORIZATION, config.avwx_auth_token)
            .send()
            .unwrap();
        if response.status().is_success() {
            let parsed: AVWXResponse = response
                .json()
                .expect("Format of the avwx API must've changed, I don't see a 'raw' key");
            println!("{}", parsed.raw);
        }
    } else {
        eprintln!(
            "You must specify avwx_auth_token in {:?}!",
            &config_file_location
        );
        let empty_config = Config {
            avwx_auth_token: String::from(
                "get a token, see https://avwx.docs.apiary.io/#introduction/authentication",
            ),
        };
        fs::write(
            config_file_location,
            toml::to_string_pretty(&empty_config).unwrap(),
        )
        .unwrap();
    }
}
